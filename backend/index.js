const express = require('express');

const app = express()

const cors = require('cors');

const mongoose = require('mongoose')
const { ApolloServer } = require('apollo-server-express');


//Files
const config = require('./src/config');
const typeDefs = require('./src/typeDefs');
const resolvers = require('./src/resolvers');

// //allow parsing of data from url
// app.use(express.urlencoded({ extended: false }))

// //allow parsing of data from a fomr
// app.use(express.json());

//allow cors
app.use(cors());

const startServer = async () => {
    const server = new ApolloServer({
        typeDefs,
        resolvers
    });
    server.applyMiddleware({ app, path: '/graphql' })

    let connection = await mongoose.connect(config.databaseURL, {
        useNewUrlParser: true, //para kapag may mga request n dadaan via url mababasa pa dn nya
        useUnifiedTopology: true, //para sa syntax
        useFindAndModify: false //pag gagamit ng mga findbyId
    });

    if (connection) console.log('Remote Database Connection Established');

    app.listen(config.PORT, () => {
        console.log(`Listening on PORT ${config.PORT}`);
    })
}

startServer();
