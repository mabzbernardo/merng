//typeDefs.js

//Type Definition:
//- it describes the data that the server will return to the client
//-it also describes the data it will accept.
//pokemonType: [PokemonType]! = it will accept []
//pokemonType: [PokemonType!]! = it won't accept [] 

const { gql } = require('apollo-server-express');

const typeDefs = gql`
    
    type Query{
        pokemons: [Pokemon]!
        items: [Item]!
        users: [User]!
        bags: [Bag]!
        myBag(userId:ID!):Bag
    }

    type Mutation{
        addPokemon(input: AddPokemonInput!): Pokemon ,
        deletePokemon(id: ID!): Pokemon,
        updatePokemon(id: ID!, input: AddPokemonInput!): Pokemon,
        addItem(input: AddItemInput!): Item,
        deleteItem(id: ID!): Item,
        updateItem(id: ID!, input: AddItemInput!): Item,
        register(input: RegisterUserInput!): User,
        login(email: String, password: String): User,
        addBag(input: AddBagInput!): Bag,
        deleteBag(id: ID!): Bag,
        addPokemonInBag(id: ID!, input: PokemonsInput!): Bag,
        addItemInBag(id: ID!, input: ItemsInput!): Bag,
        deletePokemonInBag(id: ID!, pokemonId: ID!): Bag,
        deleteItemInBag(id: ID!, itemId: ID!): Bag,
        updateItemQty( itemId: ID!, quantity: Int): Bag
    }

    type Pokemon {
        id: ID!
        name: String
        number: Int!
        gender: String!
        category: String!
        weight: Float!
        height: Float!
        description: String!
        pokemonType: [PokemonType!]!
        abilities: [Abilities!]!
        weaknesses: [Weaknesses!]!
    }

    type PokemonType {
        id: ID!
        name: String
    }

    type Abilities {
        id: ID!
        name: String
    }

    type Weaknesses {
        id: ID!
        name: String
    }

    type Item {
        id: ID!
        name: String!
        category: String!
        effect: String!
    }

    type User{
        id: ID!
        firstName: String!
        lastName: String!
        email: String!
        password: String!
        token: String!
    }

    type Bag{
        id: ID!
        items: [Items]
        pokemonList: [Pokemon]
        itemList: [Item]
        user: User
    }

    type Pokemons{
        pokemonId: ID!
    }

    type Items{
        itemId: ID!
        quantity: Int
    }

    input AddPokemonInput{
        name: String
        gender: String
        number: Int
        category: String
        weight: Float
        height: Float
        description: String
        pokemonType: [PokemonTypeInput]
        abilities: [AbilitiesInput]
        weaknesses: [WeaknessesInput]
    }

    input PokemonTypeInput{
        name: String
    }
    input AbilitiesInput{
        name: String
    }
    input WeaknessesInput{
        name: String
    }

    input AddItemInput{
        name: String
        category: String
        effect: String
    }

    input RegisterUserInput{
        firstName: String
        lastName: String
        email: String
        password: String
    }

    input AddBagInput{
        userId: String
        pokemons: [PokemonsInput]
        items: [ItemsInput]
    }

    input PokemonsInput{
        pokemonId: ID!
    }

    input ItemsInput{
        itemId: ID!
        quantity: Int
    }

`

module.exports = typeDefs;