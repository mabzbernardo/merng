const jwt = require('jsonwebtoken');
const config = require('../config')

module.exports.createToken = (user) => {
    const data = {
        _id: user._id,
        firstName: user.firstName,
        lastName: user.lastName,
        email: user.email
    };

    return jwt.sign(data, config.secret, { expiresIn: '2h' })
}