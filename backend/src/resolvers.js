//resolvers

//these are the functions that allows us to access and modify our data.

// Query, Mutation
//Query = GET requests
// Mutation = POST, PATCH, PUT, DELETE requests

const mongoose = require('mongoose');
const Pokemon = require('./models/Pokemon');
const Item = require('./models/Item');
const User = require('./models/User');
const Bag = require('./models/Bag');
const auth = require('./jwt/jwt-auth')

const bcrypt = require('bcryptjs');

const resolvers = {

    Bag: {
        //first argument = parent data
        //we want pokemons field
        //upon checking our typeDefs, we know that pokemons field is an array of pokemonId
        //now we know, pokemons is an array
        //our goal: to get the data of Pokemon based on the pokemonId
        //we mapped pokemons, for us to be able to use pokemonId
        //we look for the pokemon data using pokemonId.
        pokemonList: async ({ pokemons }, args) => {
            // console.log(parent);
            // console.log(pokemons)

            const pokemonList = pokemons.map(pokemon => {
                return Pokemon.findById(pokemon.pokemonId)
            });
            // console.log(pokemonList)
            return pokemonList;
        },
        itemList: async ({ items }) => {
            const itemList = items.map(item => {
                return Item.findById(item.itemId)
            })
            return itemList;
        },
        user: async ({ userId }) => {
            const user = await User.findById(userId)
            return user;
        }
    },

    Query: {
        pokemons: () => Pokemon.find(),
        items: () => Item.find(),
        users: () => User.find(),
        bags: () => Bag.find(),
        myBag: async (_, { userId }) => {
            const bag = await Bag.findOne({ userId: userId })
            return bag;
        }
    },
    Mutation: {
        addPokemon: async (_, { input }) => {
            // console.log(input)
            let pokemon = new Pokemon({
                name: input.name,
                number: input.number,
                gender: input.gender,
                category: input.category,
                weight: input.weight,
                height: input.height,
                description: input.description,
                pokemonType: input.pokemonType,
                abilities: input.abilities,
                weaknesses: input.weaknesses
            });

            pokemon = await pokemon.save();

            return pokemon;
        },
        //instead of args, let's destructure the parameter {id}
        deletePokemon: async (_, { id }) => {
            const pokemon = await Pokemon.findByIdAndDelete(id);
            return pokemon;
        },
        updatePokemon: async (_, { id, input }) => {
            const updates = {
                name: input.name,
                number: input.number,
                gender: input.gender,
                category: input.category,
                weight: input.weight,
                height: input.height,
                description: input.description,
                pokemonType: input.pokemonType,
                abilities: input.abilities,
                weaknesses: input.weaknesses
            }
            const pokemon = await Pokemon.findByIdAndUpdate(id, updates, { new: true })
            return pokemon;
        },
        addItem: async (_, { input }) => {
            let item = new Item({
                name: input.name,
                category: input.category,
                effect: input.effect
            })

            item = await item.save();
            return item;
        },
        deleteItem: async (_, { id }) => {
            const item = await Item.findByIdAndDelete(id);
            return item;
        },
        updateItem: async (_, { id, input }) => {
            const updates = {
                name: input.name,
                category: input.category,
                effect: input.effect
            }
            const item = await Item.findByIdAndUpdate(id, updates, { new: true })
            return item;
        },

        register: async (_, { input }) => {
            const { firstName, lastName, email, password } = input;

            const uniqueCheck = await User.find({ email });
            if (uniqueCheck.length > 0) {
                return null
            }

            let newUser = new User({
                firstName,
                lastName,
                email,
                password: bcrypt.hashSync(password, 10)
            })

            newUser = await newUser.save();

            return newUser
        },

        login: async (_, { email, password }) => {
            let user = await User.findOne({ email: email });

            if (user == null) {
                return null
            }

            const isPasswordMatched = bcrypt.compareSync(password, user.password);

            if (isPasswordMatched) {
                //if passwords matched, we'll append a token field in our user
                user.token = auth.createToken(user);
                return user;
            } else {
                return null;
            }
        },

        addBag: async (_, { input }) => {
            const { userId, pokemons, items } = input;

            let newBag = new Bag({
                userId,
                pokemons,
                items
            });

            newBag = await newBag.save();

            return newBag
        },

        addPokemonInBag: async (_, { id, input }) => {

            const updatedBag = await Bag.findByIdAndUpdate(id, { $push: { pokemons: input } }, { new: true });
            // console.log(updatedBag)
            return updatedBag;

        },

        deleteBag: async (_, { id }) => {
            const bag = await Bag.findByIdAndDelete(id);
            return bag;
        },

        addItemInBag: async (_, { id, input }) => {
            const updatedBag = await Bag.findByIdAndUpdate(id, { $push: { items: input } }, { new: true });

            return updatedBag;
        },

        deletePokemonInBag: async (_, { id, pokemonId }) => {
            const updatedBag = await Bag.findByIdAndUpdate(id, { $pull: { pokemons: { pokemonId: pokemonId } } }, { new: true })

            return updatedBag;
        },

        deleteItemInBag: async (_, { id, itemId }) => {
            const updatedBag = await Bag.findByIdAndUpdate(id, { $pull: { items: { itemId: itemId } } }, { new: true })

            return updatedBag;
        },

        updateItemQty: async (_, { itemId, quantity }) => {
            const updateBag = await Bag.findOneAndUpdate({ "items.itemId": itemId }, { $set: { 'items.$.quantity': quantity } }, { new: true });

            return updateBag;
        }
    }
}

module.exports = resolvers;