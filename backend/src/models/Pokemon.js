const mongoose = require('mongoose')

const Schema = mongoose.Schema;

const pokemonSchema = new Schema({
    name: String,
    number: Number,
    gender: String,
    pokemonType: [
        { name: String }
    ],
    category: String,
    weight: Number,
    height: Number,
    description: String,
    abilities: [{
        name: String
    }],
    weaknesses: [{
        name: String
    }]
},
    {
        timestamps: true
    })

module.exports = mongoose.model('Pokemon', pokemonSchema)