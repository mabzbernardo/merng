const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const bagSchema = new Schema({
    userId: {
        type: Schema.Types.ObjectId
    },
    pokemons: [{
        pokemonId: Schema.Types.ObjectId
    }],
    items: [
        {
            itemId: Schema.Types.ObjectId,
            quantity: Number
        }
    ]
}, {
    timestamps: true
})

module.exports = mongoose.model('Bag', bagSchema)